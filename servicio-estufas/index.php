<?php 
include '../checkip.php';
$ch = new CheckIP();
    //Example ip for localhost 
    //$user_ip = "1.1.1.19";
$user_ip = $ch->getUserIP();
$inicial = $user_ip[0];
$ch->validateIP($inicial, "estufas-inicio-nueva", $user_ip);
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Servicio para Estufas en Ciudad de México. Servicio Urgente a Domicilio en 2hrs. Centro de Servicio a Estufas. Reparación, Mantenimiento e Instalación de Estufas, Hornos y Parrillas. Teléfono: 5534821512">
  <meta name="Keywords" content="Reparación de Estufas FRIGIDAIRE, Reparación de Hornos FRIGIDAIRE, Reparación de Estufas LG, Reparación de Hornos LG, Reparación de Estufas KOBLENZ, Reparación de Hornos KOBLENZ, Reparación de Estufas TEKA, Reparación de Hornos TEKA, Reparación de Estufas MAYTAG, Reparación de Hornos MAYTAG, Reparación de Estufas WHIRLPOOL, Reparación de Hornos WHIRLPOOL, Reparación de Estufas MABE, Reparación de Hornos MABE, Reparación de Estufas WOLF, Reparación de Hornos WOLF, Reparación de Parrillas WOLF, Servicio para Estufas, Servicio para Hornos, Servicio para Parrillas, Reparación de Estufas, Reparación de Hornos, Reparación de Parrillas, Mantenimiento de Estufas, Mantenimiento de Hornos, Mantenimiento de Parrillas, Instalación de Estufas, Instalación de Hornos, Instalación de Parrillas">
  <meta name="title" content="Servicio para Estufas en CDMX | Centro Especializado | Servicio Urgente en CDMX | -10% de Descuento">
  <meta name="author" content="Reparación de Linea Blanca en México">
  <meta name="Subject" content="Reparación de Estufas a Domicilio">
  <meta name="Language" content="es">
  <meta name="Revisit-after" content="15 days">
  <meta name="Distribution" content="Global">
  <meta name="Robots" content="Index, follow">
  <meta name="theme-color" content="#000" /> 
  <title>Servicio para Estufas en CDMX | Centro Especializado | -15% de Descuento</title>


  <!-- Bootstrap core CSS -->
  <link href="../dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <link href="../carousel.css" rel="stylesheet" crossorigin="anonymous">

  <!-- Favicons -->
  <link rel="apple-touch-icon" href="../assets/img/logo-servicio-frigidaire-en-cdmx.png" sizes="180x180">
  <meta name="theme-color" content="#563d7c">

  <!-- Custom styles for this template -->
  <link href="../album.css" rel="stylesheet">
  
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160274415-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    
    gtag('config', 'UA-160274415-1');
  </script>

  <!-- Google Analytics Events-->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
    ga('create', 'UA-160274415-1', 'auto');
    ga('send', 'pageview');
  </script>
  <!-- End Google Analytics -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>
 
  <nav class="adjust-space navbar navbar-expand-md navbar-dark bg-black fixed-top">
    <a class="navbar-brand"  href="#"><img src="../assets/img/reparacion-de-estufas.png" style="width:150px;"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link btn-web-action" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')" >Mantenimiento</a>
          <a class="nav-link btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')" >Mantenimiento</a>
        </li>
        <li class="nav-item ">
          <a class="nav-link btn-web-action" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')" >Reparación</a>
          <a class="nav-link btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')" >Reparación</a>
        </li>
        <li class="nav-item ">
          <a class="nav-link btn-web-action" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')" >Instalación</a>
          <a class="nav-link btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')" >Instalación</a>
        </li>
        
      </ul>
      <div class="form-inline my-2 my-lg-0">

        <a class="btn btn-outline-light my-2 my-sm-0 btn-web-action" type="submit" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')">Agendar Visita a Domicilio</a>

        <a class="btn btn-outline-light my-2 my-sm-0 btn-mobile-action" type="submit" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')">Agendar Visita a Domicilio</a>

      </div>
    </div>
  </nav>


  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1" class=""></li>
      <li data-target="#myCarousel" data-slide-to="2" class=""></li>
      <li data-target="#myCarousel" data-slide-to="3" class=""></li>
      <li data-target="#myCarousel" data-slide-to="4" class=""></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active pwp-background-first pwp-background-first-mobile">
        <div class="container ">
          <div itemscope itemtype="http://schema.org/Dataset"  class="carousel-caption text-left">
            <h2 class="tittle-h2"><b itemprop="name">Reparación de Estufas</b></h2>
            <p class=""><span itemprop="description">Ciudad de México y Estado de México</span><br><span  style="color: #ff8f00!important;" class="descuento-bosch">!Visita técnica hoy mismo! </span></p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsslide', 'clic')" >Enviar Mensaje</a>
              <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" role="button" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a>
            </p>
          </div>
        </div> 
      </div>
      <div class="carousel-item pwp-background-second pwp-background-second-mobile">
        
        <div class="container">
          <div class="carousel-caption">
            <h2 class="tittle-h2"><b>Reparación de Hornos</b></h2>
            <p class="">Un técnico especializado te visita en tu hogar, realiza un análisis, te dice cuál es el problema y la solución, precio de la reparación y tiempo de entrega.<br><span class="descuento-bosch">Visita técnica gratis* </span> </p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsslide', 'clic')">Enviar Mensaje</a>
              <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
            </div>
          </div>
        </div>
        <div class="carousel-item pwp-background-third pwp-background-third-mobile">
          <div class="container">
            <div class="carousel-caption text-right">
              <h2 class="tittle-h2"><b>Reparación de Parrillas</b></h2>
              <p class="">Al contar con más de 20 años de experiencia brindamos Servicios de Instalación para Estufas, Hornos o Parrillas.<br><span class="descuento-bosch">Visita técnica gratis* </span></p>
              <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
                <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')" >Llamar Ahora</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item pwp-background-four pwp-background-four-mobile">
            <div class="container">
              <div class="carousel-caption text-left">
                <h2 class="tittle-h2"><b>Mantenimiento</b></h2>
                <p class="">Si tienes algún problema con tu estufa, horno o parrilla, no dudes en llamarnos, somos tu mejor opción.<br><span class="descuento-bosch">Visita técnica gratis* </span></p>
                <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
                  <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
                </div>
              </div>
            </div>

            <div class="carousel-item pwp-background-five pwp-background-five-mobile">
              
              <div class="container">
                <div class="carousel-caption">
                  <h2 class="tittle-h2"><b>Instalación</b></h2>
                  <p class="">Hacemos instalaciones a domicilio de Estufas, Hornos y Parrillas. Contamos con técnicos especialistas, servicios 100% garantizados.<br><span class="descuento-bosch">Visita técnica gratis* </span></p>
                  <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
                    <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
                  </div>
                </div>
              </div>
              
              
            </div>
            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>


          <div class="container marketing">
            <div class="row featurette">
              <div class="col-md-5 text-center" itemscope itemtype="http://schema.org/ImageObject" >
                <br class="hidde-element-mobile">
                
              </div>
              <div class="col-md-12" style="text-align: center; margin-bottom: 50px;">
                <br class="hidde-element-web">
                <h2 style="text-align: center ; font-weight: 500;
                text-transform: uppercase;
                font-size: 14px;
                color: #ff8f00!important;
                letter-spacing: 1px;
                line-height: 1.5em;">CALIDAD Y SERVICIO</h2>
                
                <h2 class="featurette-heading">Reparación y Mantenimiento <span class="text-muted">para estufas de todas las marcas.</span></h2>
                
                <a style="color: #ffffff!important;
                border-width: 0px!important;
                border-color: rgba(0,0,0,0);
                border-radius: 5px;
                letter-spacing: 1px;
                font-size: 13px;
                font-family: 'Rubik',Helvetica,Arial,Lucida,sans-serif!important;
                text-transform: uppercase!important;
                background-color: #2970fa; padding-top: 14px!important;
                padding-right: 38px!important;
                padding-bottom: 14px!important;
                padding-left: 38px!important;
                " href="https://wa.me/525567757255" class="btn btn-success my-2 shadow-lg btn-lg btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Llamar ahora</a>
                <a  href="tel:+525565020417" class="btn btn-primary mt shadow-lg btn-lg btn-mobile-action" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')">Llamar ahora</a>
              </div>
              
              <div class="col-lg-4 col-md-4 ">
                <img src="../assets/img/forma-de-trabajo.png" class="rounded" alt="reparacion y mantemiento de estufas cdmx y estado de méxico" width="100px" height="100px">
                <h2>Agendar Visita Técnica</h2>
                <p>Llamanos para agendarte una visita técnica. El técnico<strong> estara en su domicilio en menos de 2hrs</strong>* y <strong> reparara su estufa el mismo día </strong>.</p>
                
              </div>
              <div class="col-lg-4 col-md-4">
                <img src="../assets/img/obrero.png" class="rounded" alt="reparacion y mantemiento de estufas cdmx y estado de méxico" width="100px" height="100px">
                <h2>Técnicos Especializados</h2>
                <p>Nuestros técnicos están capacitados para dar<strong> soluciones inmediatas</strong> en <strong>reparación y mantenimiento de estufas</strong>. Contamos con <strong>técnicos capacitados por marca y por zona</strong>.</p>
                
              </div><!-- /.col-lg-4 -->
              <div class="col-lg-4 col-md-4 ">
                <img src="../assets/img/contrato.png" class="rounded" alt="reparacion y mantemiento de estufas cdmx y estado de méxico" width="100px" height="100px">
                <h2>Garantía por Escrito</h2>
                <p>Ofrecemos garantía por escrito para garantizar tu sasifacción con nuestro servicio.</p>
                
              </div>
            </div>
            <br><br>
          </div>
        </div>
      </div>
      <div class="container-fluid bg-pharallax bg-pharallax-m text-center">
       
        <h2 style= "color: #fff !important;"class="featurette-heading-standard"><b>¿Tu estufa no funciona?</b></h2>
        <p  style="color: #fff;"class="lead">Problemas con<b style="color: #fff;"> fugas de gas, fallas en quemadores, tuperias tapadas, perrillas rotas, perrillas desconpuestas, flama baja, flama alta, parrillas rotas, encendido de chispa y todo tipo de fallas.</b> Cuenta con nostros para <strong style="color: #ff8f00!important;">resolver el problema el mismo día hasta la puerta de tu hogar</strong>.</b><br>
          
          <a href="https://wa.me/525567757255" class="btn btn-success my-2 shadow-lg btn-lg btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Enviar Whatsapp</a>
          <a href="tel:+525565020417" class="btn btn-primary my-2 shadow-lg btn-lg btn-mobile-action" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')">Llamar Ahora</a>
          
        </p>
        
      </div>
      
      
      <div class="container">
        <br><br><br>
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <h2 style="    font-family: 'Rubik',Helvetica,Arial,Lucida,sans-serif;
            font-weight: 500;
            text-transform: uppercase;
            font-size: 14px;
            color: #ff8f00!important;
            letter-spacing: 1px;
            line-height: 1.5em;">Somos Expertos</h2>
            
            <h2 class="featurette-heading">Contamos con<span class="text-muted"> técnicos especializados en cada  marca.</span></h2>
            
            <p style="    line-height: 2em;"><strong>No importa la marca, modelo o año</strong> ya que contamos un con <strong>técnico especializado por marca</strong> que buscara dar una <strong>solucion inmedita a la reparación o mantenimiento de tu estufa</strong>.</p>
          </div>
          <div class="col-md-6 col-sm-6">
            
            <img style="max-width: 100%;
            height: auto; display: block;" src="../assets/img/reparacion-estufa-hornos-mantenimiento.png" ">
          </div>
        </div>

        <br><br><br>
      </div>
      
      <div class="container-fluid bg-pharallax-e bg-pharallax-r text-center">
       
        <div class="row">
          <div class="col-md-8 text-right">
            <h2 style="color: white;" class="featurette-heading-small"><b>¿Te urge reparar tu estufa?</b></h2>
            <p style="color: white;"class="lead">Brindamos un excelente servicio de mantenimiento preventivo o reparación para <b>estufas, hornos de cocción y parrillas a domicilio</b>. Con cobertura en la <b>CDMX y Área Metropolitana.</b><br><span style="color: #ff8f00!important;" class="descuento-bosch">20% de Descuento</span> En apoyo a tu economía en todos nuestros servicios.</span></p>
          </div>
          <div class="col-md-4 text-center">
            <div class="align-mobile">
              <a href="https://wa.me/525567757255" class="btn btn-primary mt shadow-lg btn-lg btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Agendar Visita</a>
              <a href="tel:+525565020417" class="btn btn-primary mt shadow-lg btn-lg btn-mobile-action" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')">Agendar Visita</a>
            </div>
          </div>
        </div>
        
      </div>
      
      
      <div class="container marketing">
        
        <br><br><br>
        <div class="row">
         <div class="col-12" style="text-align: center;">
          <h2 style="font-family: 'Rubik',Helvetica,Arial,Lucida,sans-serif;
          font-weight: 500;
          text-transform: uppercase;
          font-size: 14px;
          color: #ff8f00!important;
          letter-spacing: 1px;
          line-height: 1.5em;">Accesible</h2>
          
          <h2 class="featurette-heading">Formas de<span class="text-muted"> Pago.</span></h2>
          
          <p style="text-align: center;     line-height: 2em;
          ">Contamos con diferentes formas de pago así como facturacion del servicio.</p>
          <br><br>
        </div>
        <div class="col-lg-4 col-md-4 ">
          <img src="../assets/img/servicio-tecnico-para-estufas.png" class="rounded" alt="reparacion y mantemiento de estufas cdmx y estado de méxico" width="100px" height="100px">
          <h2>Pago Directo en Efectivo</h2>
          <spam style="font-family: 'Rubik',Helvetica,Arial,Lucida,sans-serif;
          line-height: 1.4em;
          text-align: center;">(Billete o Moneda)</spam>
        </div>
        <div class="col-lg-4 col-md-4">
          <img src="../assets/img/dinero.png" class="rounded" alt="reparacion y mantemiento de estufas cdmx y estado de méxico" width="100px" height="100px">
          <h2>Transferencia Bancaria</h2>
          <spam style="font-family: 'Rubik',Helvetica,Arial,Lucida,sans-serif;
          line-height: 1.4em;
          text-align: center;">(Transferencia 100% Segura)</spam>
          
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4 col-md-4 ">
          <img src="../assets/img/qVnkJzHe_400x400.png" class="rounded" alt="reparacion y mantemiento de estufas cdmx y estado de méxico" width="100px" height="100px">
          <h2>Tarjeta de Crédito y Débito</h2>
          <spam style="font-family: 'Rubik',Helvetica,Arial,Lucida,sans-serif;
          line-height: 1.4em;
          text-align: center;">(Tarjetas Visa y MasterCard)</spam>
          
        </div>
        
      </div>
      <br><br>
      <hr>
    </div>
    
    <div class="container ">
      <br><br>
      <h2 class="featurette-heading">¿Estas interesado? <span class="text-muted">Solicita una Visita.</span></h2>
      <p class="lead">Si te interesa que un técnico te visite en tu domicilio, es simple, deja tus datos en el formulario. Si deseas comunicación directa, <b><a class="text-success" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">manda un whatsapp.</a></b></p>
      <br>
      <form id="contact" class="needs-validation" novalidate>
        <div class="form-group">
          <label for="nombre">Nombre del Interesado</label>
          <input type="text" class="form-control" id="nombre" placeholder="ej: Jose Perez Lopez" name="nombre" minlength="4" maxlength="40" required pattern="^[a-zA-Z0-9 ]*$">
          <div class="valid-feedback">Correcto.</div>
          <div class="invalid-feedback">Ingresa un nombre correcto. No aceptamos puros espacios en blanco. Min 5 Max 50 caracteres. No usar (*%/$+-.,).</div>
        </div>
        <div class="form-group">
          <label for="empresa">Nombre del Negocio ó Particular</label>
          <input type="text" class="form-control" id="empresa" placeholder="ej: Limpieza Industrial" name="empresa" minlength="5" maxlength="50" required pattern="^[a-zA-Z0-9 ]*$">
          <div class="valid-feedback">Correcto.</div>
          <div class="invalid-feedback">Ingresa un numero de empresa correcto. No aceptamos puros espacios en blanco. Min 5 Max 50 caracteres. No usar (*%/$+-.,).</div>
        </div>
        <div class="form-group">
          <label for="servicio">Servicio de Interés</label>
          <select class="form-control" id="servicio" name="servicio" required>
            <option value="">Selecciona una opción...</option>
            <option>Visita a Domicilio</option>
            <option>Instalación</option>
            <option>Mantanimiento</option>
            <option>Reparación</option>
          </select>
          
          <div class="valid-feedback">Correcto.</div>
          <div class="invalid-feedback">Selecciona una opción...</div>
        </div>
        <div class="form-group">
          <label for="telefono">Teléfono</label>
          <input class="form-control" type="tel" id="telefono" name="telefono" placeholder="ej: 5554664364" pattern="[0-9]{10}" required>
          <div class="valid-feedback">Correcto.</div>
          <div class="invalid-feedback">Ingresa un teléfono correcto, de 10 dígitos.</div>
        </div>
        <div class="form-group">
          <label for="correo">Correo</label>
          <input type="email" class="form-control" id="correo" placeholder="ej: contacto@limpiezaindustrial.com.mx" name="correo" required>
          <div class="valid-feedback">Correcto.</div>
          <div class="invalid-feedback">Ingresa un correo correcto.</div>
        </div>
        <div class="form-group form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="checkbox" name="terms" required> Acepto los <span class="text-info" >Términos y Condiciones</span>.
            <div class="valid-feedback">Correcto.</div>
            <div class="invalid-feedback">Es necesario que aceptes los términos y condiciones para continuar.</div>
          </label>
        </div>
        <a onclick="sendFormEstufas(this)" class="btn btn-primary text-white shadow-lg btn-lg">Solicitar Visita</a>
      </form>
      <br><br><br>
    </div>

  </main>

  <div style=" position: fixed;" id="" class="myDiv sticky">
    <p id="tienes_dudas">
      <strong class="text-success">¿Necesitas ayuda?</strong>
      <a id="whats_link" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">
        <img src="../assets/img/servicio-de-estufas-mensaje.png" alt="" class="wp-image-31 alignnone size-medium" width="30" height="30" style="display: inline-block;">
      </a>
      <a id="llamar_link"  href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')">
        <img src="../assets/img/reparacion-de-estufas-llamada.png" alt="" class="wp-image-31 alignnone size-medium " width="30" height="30" style="display: inline-block; margin-right:35px;">
      </a>
    </p>
  </div>


  <!-- SUCCESS / FAIL ACTION MODAL -->
  
  <div class="modal fade" id="actionModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        
        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title"><p id="successModalTitle"></p></h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
          <!-- Modal body -->
          <div class="modal-body">
            <p id="successModalDescription"></p>
          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            <button class="btn btn-success" type="button" data-dismiss="modal" onclick="this.form.reset();">Continuar</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- END -->
  <!-- START ALERT -->
  <div class="toast sticky bg-primary text-white fade show" data-autohide="false">
    <div class="toast-header">
      <strong class="mr-auto text-primary">Gran Oferta</strong>
      <small class="text-muted">Servicio en 2 hrs</small>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">×</button>
    </div>
    <div class="toast-body">
      -10% de descuento. Contamos con las <span class="text-warning">medidas de sanidad necesarias</span> contra covid19. Aprovecha y agenda una visita<b><a href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')" class="text-warning btn-web-action"> dando clic aqui</a>
        <a href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'llamada', 'clic')" class="text-warning btn-mobile-action"> Agenda una visita dando clic aqui</a></b>
      </div>
    </div>
    

    <footer class="text-muted bg-black">
      <div class="container">
        <p class="float-right">
          <a href="#" class="text-warning">Volver al Principio</a>
        </p>
        <p class="text-white">Reparación de Estufas | COMODO SECURE&trade; – <b class="text-success">RapidSSL®</b></p>
        <p style="color:black">Powered by <a style="color:black" href="https://paginaswebpremium.com.mx">Páginas Web Premium</a></p>
      </div>
    </footer>

    <script type="text/javascript" src="../assets/js/letters.js"></script>
    <script type="text/javascript" src="../assets/js/form.js"></script>
    <!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
    
    <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>

    <script src="../dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>

    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>
  </body>
  </html>
