<?php 
    include '../checkip.php';
    $ch = new CheckIP();
    //Example ip for localhost 
    //$user_ip = "1.1.1.19";
    $user_ip = $ch->getUserIP();
    $inicial = $user_ip[0];
    $ch->validateIP($inicial, "estufas-inicio-nueva", $user_ip);
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Servicio para Estufas en Ciudad de México. Servicio Urgente a Domicilio en 2hrs. Centro de Servicio a Estufas. Reparación, Mantenimiento e Instalación de Estufas, Hornos y Parrillas. Teléfono: 5534821512">
    <meta name="Keywords" content="Reparación de Estufas FRIGIDAIRE, Reparación de Hornos FRIGIDAIRE, Reparación de Estufas LG, Reparación de Hornos LG, Reparación de Estufas KOBLENZ, Reparación de Hornos KOBLENZ, Reparación de Estufas TEKA, Reparación de Hornos TEKA, Reparación de Estufas MAYTAG, Reparación de Hornos MAYTAG, Reparación de Estufas WHIRLPOOL, Reparación de Hornos WHIRLPOOL, Reparación de Estufas MABE, Reparación de Hornos MABE, Reparación de Estufas WOLF, Reparación de Hornos WOLF, Reparación de Parrillas WOLF, Servicio para Estufas, Servicio para Hornos, Servicio para Parrillas, Reparación de Estufas, Reparación de Hornos, Reparación de Parrillas, Mantenimiento de Estufas, Mantenimiento de Hornos, Mantenimiento de Parrillas, Instalación de Estufas, Instalación de Hornos, Instalación de Parrillas">
    <meta name="title" content="Servicio para Estufas en CDMX | Centro Especializado | Servicio Urgente en CDMX | -10% de Descuento">
    <meta name="author" content="Reparación de Linea Blanca en México">
    <meta name="Subject" content="Reparación de Estufas a Domicilio">
    <meta name="Language" content="es">
    <meta name="Revisit-after" content="15 days">
    <meta name="Distribution" content="Global">
    <meta name="Robots" content="Index, follow">
    <meta name="theme-color" content="#222740" /> 
    <title>Servicio para Estufas en CDMX | Centro Especializado | -15% de Descuento</title>


    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link href="../carousel.css" rel="stylesheet" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="../assets/img/logo-servicio-frigidaire-en-cdmx.png" sizes="180x180">
    <meta name="theme-color" content="#563d7c">

    <!-- Custom styles for this template -->
    <link href="../album.css" rel="stylesheet">
  
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160274415-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-160274415-1');
    </script>

    <!-- Google Analytics Events-->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
    ga('create', 'UA-160274415-1', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

  </head>
  <body>
     
  <header class="sticky-top position"  >
  <div class="bg-bluemarine collapse" id="navbarHeader" style="">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-md-7 py-4">
          <h4 class="text-white">Reparación de Estufas a Domicilio</h4>
          <p class="text-white">Si se descompuso tu estufa, horno o parrilla, no dudes en comunicarte con nosotros y pedir la visita de uno de nuestros técnicos en la comodidad de tu hogar.</p>
        </div>
        <div class="col-sm-4 offset-md-1 py-4">
          <h4 class="text-white">Contacto</h4>
          <ul class="list-unstyled">
            <li><a href="https://wa.me/525567757255" class="text-white" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')">Agendar una Visita</a></li>
            <li><a href="https://wa.me/525567757255" class="text-white" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')">Servicio Urgente</a></li>
            <li><a href="https://wa.me/525567757255" class="text-white" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')">Enviar Whatsapp</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="navbar navbar-light bg-white shadow-sm ">
    <div class="container d-flex justify-content-between">
      <a href="#" class="navbar-brand d-flex align-items-center">
        <img src="../assets/img/reparacion-de-estufas-cdmx-logo.png" class="img-logo-size" alt="reparacion de estufas en cdmx" >
      </a>

      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon my-toggler"></span>
      </button>
    </div>
  </div>
</header>

<main role="main">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1" class=""></li>
      <li data-target="#myCarousel" data-slide-to="2" class=""></li>
      <li data-target="#myCarousel" data-slide-to="3" class=""></li>
      <li data-target="#myCarousel" data-slide-to="4" class=""></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active pwp-background-first pwp-background-first-mobile">
        <div class="container ">
          <div itemscope itemtype="http://schema.org/Dataset"  class="carousel-caption text-left">
            <h2 class="tittle-h2"><b itemprop="name">Reparación de Estufas</b></h2>
            <p class="text"><span itemprop="description">Contamos con técnicos especializados en diferentes marcas: MABE, LG, SAMSUNG, WHIRLPOOL, MAYTAG, FRIGIDAIRE, GENERAL ELECTRIC, KOBLENZ, SUBZERO, WOLF, BOSCH y más...</span><br><span class="descuento-bosch">Visita técnica gratis* </span></p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsslide', 'clic')" >Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" role="button" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a>
          </p>
          </div>
        </div> 
      </div>
      <div class="carousel-item pwp-background-second pwp-background-second-mobile">
        
        <div class="container">
          <div class="carousel-caption">
            <h2 class="tittle-h2"><b>Reparación de Hornos</b></h2>
            <p class="text">Un técnico especializado te visita en tu hogar, realiza un análisis, te dice cuál es el problema y la solución, precio de la reparación y tiempo de entrega.<br><span class="descuento-bosch">Visita técnica gratis* </span> </p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item pwp-background-third pwp-background-third-mobile">
        <div class="container">
          <div class="carousel-caption text-right">
            <h2 class="tittle-h2"><b>Reparación de Parrillas</b></h2>
            <p class="text">Al contar con más de 20 años de experiencia brindamos Servicios de Instalación para Estufas, Hornos o Parrillas.<br><span class="descuento-bosch">Visita técnica gratis* </span></p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')" >Llamar Ahora</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item pwp-background-four pwp-background-four-mobile">
        <div class="container">
          <div class="carousel-caption text-left">
            <h2 class="tittle-h2"><b>Mantenimiento</b></h2>
            <p class="text">Si tienes algún problema con tu estufa, horno o parrilla, no dudes en llamarnos, somos tu mejor opción.<br><span class="descuento-bosch">Visita técnica gratis* </span></p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
          </div>
        </div>
      </div>

      <div class="carousel-item pwp-background-five pwp-background-five-mobile">
        
        <div class="container">
          <div class="carousel-caption">
            <h2 class="tittle-h2"><b>Instalación</b></h2>
            <p class="text">Hacemos instalaciones a domicilio de Estufas, Hornos y Parrillas. Contamos con técnicos especialistas, servicios 100% garantizados.<br><span class="descuento-bosch">Visita técnica gratis* </span></p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525565020417" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
          </div>
        </div>
      </div>
      
      
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="container marketing">
    <div class="row featurette">
      <div class="col-md-5 text-center" itemscope itemtype="http://schema.org/ImageObject" >
        <br class="hidde-element-mobile">
        <br class="hidde-element-mobile">
        <img class="img-size-png" src="../assets/img/descuento.png" alt="reparacion-mantenimietnto-instalacion-de-estufas" itemprop="contentUrl">
      </div>
      <div class="col-md-7">
        <br class="hidde-element-web"><br class="hidde-element-web">
        <h2 class="featurette-heading-standard">Reparación y Mantenimiento <span class="text-muted">de Estufas, Servicio Urgente en Ciudad de México y Área Metropolitana.</span></h2>
        <p class="lead">Si tienes una estufa, un horno ó una parrilla descompuesta, nosotros te podemos ayudar, somos un Centro de Servicio Especializado con Experiencia en Diferentes Marcas: <b>MABE, LG, SAMSUNG, WHIRLPOOL, MAYTAG, FRIGIDAIRE, GENERAL ELECTRIC, KOBLENZ, SUBZERO, WOLF, BOSCH </b>y más... con cobertura en Ciudad de México y Área Metropolitana.</p><br>
      </div>
    </div>
    <br>
    <br><br>
  </div>
    <div class="container-fluid bg-pharallax bg-pharallax-m text-center">
   
      <h2 style= "color: #fff !important;"class="featurette-heading-standard"><b>¿Tu estufa no funciona?</b></h2>
      <p  style="color: #fff;"class="lead">Problemas con<b style="color: #fff;"> fugas de gas, fallas en quemadores, tuperias tapadas, perrillas rotas, perrillas desconpuestas, flama baja, flama alta, parrillas rotas, encendido de chispa y todo tipo de fallas.</b> Cuenta con nostros para <span class="visita">resolver el problema el mismo día hasta la puerta de tu hogar.</span></b><br>
    
        <a href="https://wa.me/525567757255" class="btn btn-success my-2 shadow-lg btn-lg btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Enviar Whatsapp</a>
        <a href="tel:+525565020417" class="btn btn-primary my-2 shadow-lg btn-lg btn-mobile-action" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')">Llamar Ahora</a>
        
      </p>
   
  </div>
    
 
  <div class="container">
    <br><br><br>
  <div class="row text-center">
      <div class="col-md-12"><h2 class="featurette-heading-small text-muted">Somos Expertos</h2>
      <p class="lead">Actualmente tenemos experiencia en la instalación, mantenimiento y reparación de estufas, hornos y parrillas de diferentes marcas: <b>Frigidaire, Maytag, WOLF, Koblenz, Mabe, Samsung, LG, GE, Sub Zero, Whirlpool</b> y más...</p>
      </div>
      <div class="col-md-3 col-sm-6 col-xs efecto"><img src="../assets/img/frigidaire-logo.png" class="frigidaire" width="180px" alt="diseno-de-paginas-web"></div>
      <div class="col-md-3 col-sm-6 col-xs efecto"><img src="../assets/img/logo-maytag.png" class="maytag" width="125px" alt="diseno-de-paginas-web"></div>
      <div class="col-md-3 col-sm-6 col-xs efecto"><img src="../assets/img/whirlpool-logo.png" class="whirlpool" width="110px" alt="diseno-de-paginas-web"></div>
      <div class="col-md-3 col-sm-6 col-xs efecto"><img src="../assets/img/mabe-logo.png" class="mabe" width="120px" alt="diseno-de-paginas-web"></div>
      

      
      <div class="col-md-12 hidde-element-mobile"><br></div>
      
      <div class="col-md-3 col-sm-6 col-xs efecto"><img src="../assets/img/koblenz-logo.jpg" class="koblenz" width="180px" alt="diseno-de-paginas-web"></div>
      <div class="col-md-3 col-sm-6 col-xs efecto"><img src="../assets/img/lg-logo.png" class="lg" width="90px" alt="diseno-de-paginas-web"></div>
      <div class="col-md-3 col-sm-6 col-xs efecto"><img src="../assets/img/wolf-logo.png" class="wolf" width="130px" alt="diseno-de-paginas-web"></div>
      <div class="col-md-3 col-sm-6 col-xs efecto"><img src="../assets/img/teka-logo.png" class="teka" width="76px" alt="diseno-de-paginas-web"></div>
      <div class="col-md-3 col-sm-6 col-xs efecto"></div>
       <div class="col-md-3 col-sm-6 col-xs efecto"><img src="../assets/img/bosch-logo.png" style="padding-top: 30px;" class="teka" width="150px" alt="diseno-de-paginas-web"></div>
       <div class="col-md-3 col-sm-6 col-xs efecto"><img src="../assets/img/logo-samsung.png" style="padding-top: 35px;" class="teka" width="150px" alt="diseno-de-paginas-web"></div>
      
      
  </div>
  <br><br><br>
  </div>
  
<div class="container-fluid bg-pharallax-e bg-pharallax-r text-center">
   
      <div class="row">
      <div class="col-md-8 text-right">
      <h2 style="color: white;" class="featurette-heading-small"><b>¿Te urge reparar tu estufa?</b></h2>
      <p style="color: white;"class="lead">Brindamos un excelente servicio de mantenimiento preventivo o reparación para <b>estufas, hornos de cocción y parrillas a domicilio</b>. Con cobertura en la <b>CDMX y Área Metropolitana.</b><br><span class="descuento-bosch">20% de Descuento</span> En apoyo a tu economía en todos nuestros servicios.</span></p>
    </div>
    <div class="col-md-4 text-center">
      <div class="align-mobile">
        <a href="https://wa.me/525567757255" class="btn btn-primary mt shadow-lg btn-lg btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Agendar Visita</a>
        <a href="tel:+525565020417" class="btn btn-primary mt shadow-lg btn-lg btn-mobile-action" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')">Agendar Visita</a>
      </div>
    </div>
    </div>
   
  </div>
 
  
   <div class="container marketing">
        
    <br><br><br>
    <div class="row">
       <div class="col-12" style="text-align: center;">
      <h2 class="featurette-heading-small">¿Porqué Contratarnos?
      </h2>
      <p class="lead">Es simple, porque nosotros ofrecemos alta calidad en nuestros <b>servicios en reparación, instalación y mantenimiento</b> de estufas, hornos de cocción y parrillas, tanto de hogares como industriales.</p>
      <br><br>
    </div>
      <div class="col-lg-3 col-md-3 ">
          <img src="../assets/img/servicio-a-domicilio-reparacion-de-estufas.png" class="rounded" alt="diseno-de-paginas-web" width="100px" height="100px">
          <h2>Servicio a Domicilio</h2>
          <p>Nuestros técnicos te visitan en la comodidad de tu hogar, negocio ó empresa.</p>
          
      </div>
      <div class="col-lg-3 col-md-3">
        <img src="../assets/img/piezas-originales-reparacion-de-estufas.png" class="rounded" alt="diseno-de-paginas-web" width="100px" height="100px">
        <h2>Refacciones Originales</h2>
        <p>Todas nuestras refacciones son originales, nosotros no fomentamos el uso de piratería.</p>
        
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-3 col-md-3 ">
        <img src="../assets/img/garantia-de-satisfaccion-reparacion-de-estufas.png" class="rounded" alt="diseno-de-paginas-web" width="100px" height="100px">
        <h2>Garantía de Calidad</h2>
        <p>Todos nuestros servicios son realizados por técnicos especialistas.</p>
        
      </div>
      <div class="col-lg-3 col-md-3 ">
        <img src="../assets/img/limpieza.png" class="rounded" alt="diseno-de-paginas-web" width="100px" height="100px">
        <h2>Sanitización</h2>
        <p>Nuestras herramientas como unidades son sanitizadas. Nuestros técnicos pasan por un chequeo medico diario para salvaguardar su salud.</p>
        
      </div><!-- /.col-lg-4 -->
      <!-- /.col-lg-4 -->
    </div>
      <br><br>
    </div>
  
  <div class="container-fluid bg-bluemarine">
    <div class="row">

      <div class="col-md-1 hidden-xs-down"></div>
      <div class="col-md-10 text-white" style="text-align: center;">
        <br><br>
        <h2 class="featurette-heading-standard">Preguntas Frecuentes
        </h2>
        <p class="lead">A continuación listamos una pequeña serie de preguntas que hemos identificado como frecuentes o muy frecuentes, cualquier observación o comentario, no dudes y <b><a class="text-success" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">envia un mensaje </a></b></p>
        <br><br>
      </div>
      <div class="col-md-1 hidden-xs-down"></div>

      <div class="col-md-1"></div>
      <div class="col-md-4 col-sm-6">
        <div id="list-example" class="list-group">
          <a class="list-group-item list-group-item-action active disabled" href="#list-item-1">¿Qué precio tiene la visita a mi domicilio?</a>
          <a class="list-group-item list-group-item-action disabled" href="#list-item-2">¿El precio total incluye refacciones?<a>
          <a class="list-group-item list-group-item-action disabled" href="#list-item-3">¿Cual es su red de cobertura actúal?</a>
          <a class="list-group-item list-group-item-action disabled" href="#list-item-4">¿Me dan garatía del servicio?</a>
          <a class="list-group-item list-group-item-action disabled" href="#list-item-5">¿Qué metodos de pago aceptamos?</a>
        </div>
        <br>
      </div>

      <div class="col-md-6 col-sm-6 text-white shadow-ask">
        <div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example">
          <h4 id="list-item-1">¿Qué precio tiene la visita a mi domicilio?</h4>
          <p>La visita no tiene ningun costo, <b>siempre y  cuando acepte el presupuesto que nuestros técnicos le ofrezcan para la reparación de su electrodoméstico.</b> En caso contrario la cisita tiene un costo de $150 pesos en la CDMX y $350 pesos en el Estado de México.</p>
          <h4 id="list-item-2">¿El precio total incluye refacciones?</h4>
          <p>Asi es, el precio total incluye piezas y también te brindamos de paso una garantía que asegura la calidad y los buenos resultados de nuestros servicios.<b>Recuerda que contamos con servicio a domicilio</b> y si te animas a reparar tu equipo, la visita no tiene costo, es completamente gratis.</p>
          <h4 id="list-item-3">¿Cual es su red de cobertura actúal?</h4>
          <p>Actualmente tenemos cobertura en toda la CDMX y sus 16 Alcaldias <b>(Álvaro Obregón, Azcapotzalco, Benito Juárez, Coyoacán, Cuajimalpa de Morelos, Cuauhtémoc, Gustavo A. Madero, IztacalcoI, ztapalapa, Magdalena Contreras, Miguel Hidalgo, Milpa Alta, Tláhuac, Tlalpan, Venustiano Carranza, Xochimilco)</b> y Área Metropolitana.</p>
          <h4 id="list-item-4">¿Me dan garatía del servicio?</h4>
          <p>Claro, La garantía te la entregamos en papel, con un formato impreso y también con un registro que llevamos de nuestros servicios en sistema.</b> Entonces tu garantía estará disponible también en la bandeja de entrada de tu correo personal si asi lo deseas.</b></p>
          <h4 id="list-item-5">¿Que métodos de pago aceptamos?</h4>
          <p>Actualmente recibimos todas las tarjetas de <b>Credito y Débito</b>. Por mencionar algunas: <b>VISA, AMERICAN EXPRESS, MASTER CARD, MAESTRO y más...</b> La idea principal es tener la oportunidad de trabajar contigo y abrir el catálogo de oportunidades que tenemos disponibles para ti.</p>
        </div>
      </div>
      <div class="col-md-1"></div>
    </div>
     <br><br><br><br>
  </div>


      <br><br>
  <div class="container ">
    <h2 class="featurette-heading">¿Estas interesado? <span class="text-muted">Solicita una Visita.</span></h2>
    <p class="lead">Si te interesa que un técnico te visite en tu domicilio, es simple, deja tus datos en el formulario. Si deseas comunicación directa, <b><a class="text-success" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">manda un whatsapp.</a></b></p>
    <br>
    <form id="contact" class="needs-validation" novalidate>
    <div class="form-group">
      <label for="nombre">Nombre del Interesado</label>
      <input type="text" class="form-control" id="nombre" placeholder="ej: Jose Perez Lopez" name="nombre" minlength="4" maxlength="40" required pattern="^[a-zA-Z0-9 ]*$">
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un nombre correcto. No aceptamos puros espacios en blanco. Min 5 Max 50 caracteres. No usar (*%/$+-.,).</div>
    </div>
    <div class="form-group">
      <label for="empresa">Nombre del Negocio ó Particular</label>
      <input type="text" class="form-control" id="empresa" placeholder="ej: Limpieza Industrial" name="empresa" minlength="5" maxlength="50" required pattern="^[a-zA-Z0-9 ]*$">
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un numero de empresa correcto. No aceptamos puros espacios en blanco. Min 5 Max 50 caracteres. No usar (*%/$+-.,).</div>
    </div>
    <div class="form-group">
      <label for="servicio">Servicio de Interés</label>
      <select class="form-control" id="servicio" name="servicio" required>
        <option value="">Selecciona una opción...</option>
        <option>Visita a Domicilio</option>
        <option>Instalación</option>
        <option>Mantanimiento</option>
        <option>Reparación</option>
      </select>
      
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Selecciona una opción...</div>
    </div>
    <div class="form-group">
      <label for="telefono">Teléfono</label>
      <input class="form-control" type="tel" id="telefono" name="telefono" placeholder="ej: 5554664364" pattern="[0-9]{10}" required>
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un teléfono correcto, de 10 dígitos.</div>
    </div>
    <div class="form-group">
      <label for="correo">Correo</label>
      <input type="email" class="form-control" id="correo" placeholder="ej: contacto@limpiezaindustrial.com.mx" name="correo" required>
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un correo correcto.</div>
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="terms" required> Acepto los <span class="text-info" >Términos y Condiciones</span>.
        <div class="valid-feedback">Correcto.</div>
        <div class="invalid-feedback">Es necesario que aceptes los términos y condiciones para continuar.</div>
      </label>
    </div>
    <a onclick="sendForm(this)" class="btn btn-primary text-white shadow-lg btn-lg">Solicitar Visita</a>
  </form>
  <br><br><br>
  </div>

</main>

<!-- SUCCESS / FAIL ACTION MODAL -->
  
  <div class="modal fade" id="actionModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title"><p id="successModalTitle"></p></h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
        <!-- Modal body -->
        <div class="modal-body">
          <p id="successModalDescription"></p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal" onclick="this.form.reset();">Continuar</button>
        </div>
        </form>
      </div>
    </div>
  </div>

<footer class="text-muted bg-black">
  <div class="container">
    <p class="float-right">
      <a href="#" class="text-warning">Volver al Principio</a>
    </p>
    <p class="text-white">Reparación de Estufas | COMODO SECURE&trade; – <b class="text-success">RapidSSL®</b></p>
    <p style="color:black">Powered by <a style="color:black" href="https://paginaswebpremium.com.mx">Páginas Web Premium</a></p>
  </div>
</footer>

  <script type="text/javascript" src="../assets/js/letters.js"></script>
  <script type="text/javascript" src="../assets/js/form.js"></script>
  <!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
        
  <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>

  <script src="../dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>

  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>
</body>
</html>
